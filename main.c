/*
*    COMP30017 Operating Systems and Network Services
*    Semester 1, 2013
*    Project 2
*
*    Team members:
*        Jonathan Adhitama Wongsodihardjo        jawo    531621
*        Michael Jonathan                        jonm    384881
*
*    Main server module, receives connections from clients and assigns
*    them to threads.
*
*/

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include "socket.h"
#include "maildir.h"
#include "logging.h"
#include "pop.h"

#define LISTEN_BACKLOG 100

void *pop_thread_runner(void * param);

char last_char_of(char* str)
{
	return str[strlen(str)-1];
}

int main(int argc, char **argv){

	if((argc != 4) || (last_char_of(argv[2]) != '/'))
	{
		printf("USAGE: pop3 [port] [maildir path] [log file]\n");
		printf("NOTE: the maildir path must end with an '/'\n");
		exit(1);
	}
	
	// Set up server socket
	int socketfd = socket_setup(atoi(argv[1]));
	
	// Set maildir path
	dir_set_path(argv[2]);
	
	// Set up logging
	log_setUp(argv[3]);
	
	// Start listening
	listen(socketfd, LISTEN_BACKLOG);
	
	// Log
	log_write("Main", "POP3 Server Started", "", "");
	
	// Accept clients
	int clisocketfd;
	pthread_t threadid;
	while(1)
	{
		// Receive connection
		clisocketfd = socket_get_new_connection(socketfd);
		// Log
		log_write("Main", "New Connection", "", "");
		// Create thread
		if (pthread_create(&threadid, NULL, pop_thread_runner, (void *) clisocketfd) < 0)
		{
			perror("main: ERROR: Can't create thread for client.\n");
			close_socket(clisocketfd);
			continue;
		}
	}
}

void *pop_thread_runner(void * clisocketfd)
{
	pop_protocol((int) clisocketfd);
}
