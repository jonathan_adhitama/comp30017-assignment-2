pop3: main.o pop.o socket.o maildir.o logging.o smalloc.o
	gcc -lpthread -lrt -lsocket -lnsl -o pop3 main.o pop.o socket.o maildir.o logging.o smalloc.o

main.o: main.c pop.h socket.h maildir.h logging.h 
	gcc -c -lpthread -lrt -lsocket -lnsl main.c

pop.o: pop.c pop.h socket.h maildir.h logging.h 
	gcc -c -lpthread -lrt -lsocket -lnsl pop.c

socket.o: socket.c socket.h 
	gcc -c -lpthread -lrt -lsocket -lnsl socket.c

maildir.o:  maildir.c maildir.h logging.h 
	gcc -c -lpthread -lrt -lsocket -lnsl maildir.c

logging.o: logging.c logging.h 
	gcc -c -lpthread -lrt -lsocket -lnsl logging.c
	
smalloc.o:	smalloc.c
	gcc -c smalloc.c

clean: 
	rm *.o
