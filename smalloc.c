/*
*   COMP30017 Operating Systems and Network Services
*   Semester 1, 2013
*   Project 2
*
*   Team members:
*       Jonathan Adhitama Wongsodihardjo        jawo    531621
*       Michael Jonathan                        jonm    384881
*
*   Functions for safe malloc, safe realloc, and safe free.
*
*/

#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

void* smalloc(size_t size)
{
	assert(size != 0);
	void* temp = malloc(size);
	assert(temp != NULL);
	return temp;
}

void* srealloc(void *ptr, size_t size)
{
	assert(ptr != NULL);	
	assert(size != 0);
	void* temp = realloc(ptr, size);
	assert(temp != NULL);
	return temp;
}

void sfree(void* ptr)
{
	assert(ptr != NULL);
	free(ptr);
	return;
}
