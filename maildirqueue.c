/*
*    COMP30017 Operating Systems and Network Services
*    Semester 1, 2013
*    Project 2
*
*    Team members:
*        Jonathan Adhitama Wongsodihardjo        jawo    531621
*        Michael Jonathan                        jonm    384881
*
*    Queue module, designed to be used while traversing a directory
*    to later create a FileStruct for that directory.
*
*/

#include <assert.h>
#include <strings.h>
#define BUFFER_SIZE 512

void* smalloc(size_t size);
void* srealloc(void *ptr, size_t size);
void sfree(void* ptr);

typedef struct mailfilenode {
	char filename[BUFFER_SIZE];
	int filesize;
	struct mailfilenode * next;
} mnode;

typedef struct mailqueue {
	mnode * first;
	mnode * last;
} mqueue; 

void mqueue_push(mqueue * queue, char * name, int size) {
	assert(queue != NULL);
	mnode * to_insert = (mnode *) smalloc(sizeof(mnode));
	strncpy(to_insert->filename, name, BUFFER_SIZE);
	to_insert->filesize = size;
	to_insert->next = NULL;
	queue->last->next = to_insert;
	queue->last = to_insert;
}

mnode * mqueue_pop(mqueue * queue) {
	assert(queue != NULL);
	mnode * to_return = queue->first->next;
	if (to_return == NULL) {
		return NULL;
	}
	else {
		queue->first->next = to_return->next;
		return to_return;
	}
}

mqueue * mqueue_init() {
	mnode * dummy = (mnode *) smalloc(sizeof(mnode));
	dummy -> next = NULL;
	mqueue * queue = (mqueue *) smalloc(sizeof(mqueue));
	queue->first = dummy;
	queue->last = dummy;
	return queue;
}

void mqueue_destroy(mqueue * queue) {
	assert(queue != NULL);
	mnode * current = queue->first;
	mnode * next;
	while (current != NULL) {
		next = current->next;
		free(current);
		current = next;
	}
	free(queue);
}
