/*
*   COMP30017 Operating Systems and Network Services
*   Semester 1, 2013
*   Project 2
*
*   Team members:
*       Jonathan Adhitama Wongsodihardjo        jawo    531621
*       Michael Jonathan                        jonm    384881
*
*   Contain functions to write to log files.
*
*/

#include "logging.h"

#include <stdio.h>
#include <pthread.h>
#include <time.h>

#define TRUE 1
#define FALSE 0

#define TIME_BUF 100

static FILE* logfile;
pthread_mutex_t loglock;
char log_ready = FALSE;

/*
* This function is to be called once, 
* before the first call to log_write is made.
*  
* log_file_name is the path and file name of the file to be used for logging.
*/
void log_setUp(char* log_file_name)
{
	// Open log file
	logfile = fopen(log_file_name, "a");
	if (logfile < 0)
	{
		perror("Can't open log file.");
		exit(1);
	}
	// Create mutex lock
	if (pthread_mutex_init(&loglock, NULL) != 0)
	{
		perror("Can't initialize log mutex.");
		exit(1);
	}
	log_ready = TRUE;
}

/*
* Writes a log entry to the log file.
* entries must be formatted with this pattern: "%s: %s: %s: %s: %s\n"
* From left to right the %s are to be:
*  - a time stamp, in local time.
*  - module (which part of the program created the entry?)
*  - p1
*  - p2
*  - p3
* 
* Note: This function may be called from multiple threads and hence must
*       ensure that only one thread is writing to the file at any given time.
*/
void log_write(char* module, char* p1, char* p2, char* p3)
{
	// Get current time first
	time_t now = time(NULL);
	
	// Make sure log is ready
	if (!log_ready)
	{
		fprintf(stderr, "ERROR: Can't write log, logfile not set up.\n");
		pthread_exit(NULL);
	}
	
	// Prepare timestamp
	char time_buffer[TIME_BUF];
	strncpy(time_buffer, ctime(&now), sizeof(time_buffer));
	time_buffer[strlen(time_buffer)-1] = '\0'; // remove newline
	
	// Write log
	pthread_mutex_lock(&loglock);
	fprintf(logfile, "%s: %s: %s: %s: %s\n", time_buffer, module, p1, p2, p3);
	fflush(logfile);
	pthread_mutex_unlock(&loglock);
}

