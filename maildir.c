/*
*   COMP30017 Operating Systems and Network Services
*   Semester 1, 2013
*   Project 2
*
*   Team members:
*       Jonathan Adhitama Wongsodihardjo        jawo    531621
*       Michael Jonathan                        jonm    384881
*
*   Mail directory functions.
*
*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h> 
#include <sys/stat.h>

#include "maildir.h"
#include "logging.h"

#include "maildirqueue.c"

#define BUF_SIZE 512
#define PASSWORD_FILENAME "pass"

void* smalloc(size_t size);
void* srealloc(void *ptr, size_t size);
void sfree(void* ptr);

static char mdpath[BUF_SIZE];

/*
* This function is to be called once at the beginning
* to set the root path of the maildirs.
*/
void dir_set_path(char* path){
    strncpy(mdpath, path, BUF_SIZE);
}

/*
* This function checks the given username and password
* against the stored values.
*
* If the username is valid (has a corresponding folder)
* and the given password matches the pass file 
* then the function should return 'true'.
*
* Otherwise it is to return 'false'.
*
* Note 'true' and 'false' are defined in maildir.h
*/
bool check_user(char* username, char* pass)
{
    char path[BUF_SIZE];
    char password_read[BUF_SIZE];
    
    // Open password file
    snprintf(path, sizeof(path), "%s%s/pass", mdpath, username);
    FILE* file = fopen(path, "r");
    if (!file)
    {
        return false;
    }
    
    // Read password file
    char* fgets_status = fgets(password_read, BUF_SIZE, file);
    fclose(file);
    if (!fgets_status)
    {
        perror("ERROR: cannot read the password file\n");
        pthread_exit(NULL);
    }
    password_read[strlen(password_read)-1] = '\0';
    
    // Verify password
    return (strcmp(password_read, pass) == 0);
}

/*
* Constructs a FilesStruct as described in maildir.h
* for the maildir of the given user.
*/
FilesStruct* dir_get_list(char* user)
{
    // Log
    log_write("MailDir", user, "GetList", "");
    
    // Construct directory path
    char dir_path[BUF_SIZE];
    snprintf(dir_path, sizeof(dir_path), "%s%s/", mdpath, user);
    
    // Open directory
    DIR *directory = opendir(dir_path);
    if (!directory)
    {
        perror("ERROR: Username does not exist.\n");
        pthread_exit(NULL);
    }
    
    // Prepare to push files into queue
    struct dirent *dir;
    struct stat file_stat;
    char file_path[BUF_SIZE];
    
    mqueue* mailqueue = mqueue_init();
    int count = 0;
    
    // Push files into queue
    while ((dir = readdir(directory)) != NULL)
    {
        // Construct file path
        snprintf(file_path, sizeof(file_path), "%s%s", dir_path, dir->d_name);
        stat(file_path, &file_stat);
        
        // Skip directories and password files
        if (S_ISDIR(file_stat.st_mode) || strcmp(dir->d_name, PASSWORD_FILENAME) == 0)
        {
            continue;
        }
        
        // Push to queue
        mqueue_push(mailqueue, dir->d_name, file_stat.st_size);
        count++;
    }

    // Prepare to pull files from queue to FilesStruct
    FilesStruct * files_struct;
    files_struct = (FilesStruct *) smalloc(sizeof(FilesStruct));
    files_struct->FileNames = (char **) smalloc(sizeof(char *) * count);
    files_struct->FileSize = (int *) smalloc(sizeof(int) * count);
    files_struct->count = count;
    
    mnode * node;
    int filename_length;
    int i;
    
    // Pull files from queue to FilesStruct
    for (i = 0; (node = mqueue_pop(mailqueue)) != NULL; i++)
    {
        filename_length = strlen(node->filename);
        files_struct->FileNames[i] = (char *) smalloc((sizeof(char) * filename_length) + 1);
        strncpy(files_struct->FileNames[i], node->filename, filename_length + 1);
        files_struct->FileNames[i][filename_length] = '\0';
        files_struct->FileSize[i] = node->filesize;
    }
    
    mqueue_destroy(mailqueue);
    
    return files_struct;
}

/*
* Delete the given file from the maildir of the given user.
*/
void delete_mail(char* user, char* filename)
{
    // Log
    log_write("MailDir", user, "Delete", filename);
    
    // Construct path
    char path[BUF_SIZE];
    snprintf(path, sizeof(path), "%s%s/%s", mdpath, user, filename);
    
    // Delete
    int status = remove(path);
    if (status != 0) {
        perror("File to delete does not exist!\n");
        pthread_exit(NULL);
    }
}

/*
*	Helper function for get_file. Returns true if a string ends with CRLF. 
*/
bool string_ends_with_crlf(char *str)
{
    int len = strlen(str);
    if (len < 2)
    {
        return false;
    }
    return ((str[len-2]=='\r') && (str[len-1]=='\n'));
}

/*
* Returns the contents of a given file from a user's maildir.
*
* This function MUST also byte-stuff the termination character 
* as described in the project spec.
*
* This function MUST also append the termination character (`.') and a CRLF pair 
* to the end.
*/
char* get_file(char* user, char* filename)
{
    // Log
    log_write("MailDir", user, "GetFile", filename);
    
    // Open file
    char path[BUF_SIZE];
    snprintf(path, sizeof(path), "%s%s/%s", mdpath, user, filename);
    FILE *file = fopen(path, "r");
    if (!file)
    {
        perror("ERROR: Can't get file.");
        pthread_exit(NULL);
    }
    
    // Make space
    int mail_size = 1;
    char* mail_buf = (char*) smalloc(sizeof(char) * mail_size);
    mail_buf[0] = '\0';
    
    char read_buf[BUF_SIZE];
    int len;
    
    while (1)
    {
        // Reset buffer
        bzero(read_buf, sizeof(read_buf));
        
        // Read line from file to read buffer
        if (fgets(read_buf, sizeof(read_buf), file) == NULL)
        {
            break;
        }
        
        // Byte-stuff
        if ((read_buf[0] == '.') && (mail_size >= 2) && (string_ends_with_crlf(mail_buf)))
        {
            mail_size += 1;
            mail_buf = (char*) srealloc(mail_buf, sizeof(char) * mail_size);
            strncat(mail_buf, ".", 1);
        }
        
        // Copy read buffer to mail buffer
        len = strlen(read_buf);
        mail_size += len;
        mail_buf = (char*) srealloc(mail_buf, sizeof(char) * mail_size);
        strncat(mail_buf, read_buf, len);
    }
    
    // Close file
    fclose(file);
    
    // Append end
    mail_size += 3;
    mail_buf = (char*) srealloc(mail_buf, sizeof(char) * mail_size);
    strncat(mail_buf, ".\r\n", 3);
    
    // Return (mail_buf must be freed later)
    return mail_buf;
}
