/*
*    COMP30017 Operating Systems and Network Services
*    Semester 1, 2013
*    Project 2
*
*    Team members:
*        Jonathan Adhitama Wongsodihardjo        jawo    531621
*        Michael Jonathan                        jonm    384881
*
*    POP3 server thread. Follows the standard POP3 protocol.
*
*/

#include "pop.h"
#include "socket.h"
#include "maildir.h"
#include "logging.h"

#include <string.h>
#include <strings.h>

#define maxUserLen 50
#define maxPassLen 50
#define BUF_SIZE 512
#define ARG_SIZE 40
#define CMD_SIZE 4

void* smalloc(size_t size);
void* srealloc(void *ptr, size_t size);
void sfree(void* ptr);

void state_authorization(int cfd, char * username);
void state_transaction(int connection_descriptor, char * username, FilesStruct * fs, char * delete_todo);
void state_update(int connection_descriptor, char * username, FilesStruct * fs, char * delete_todo);

void exit_pop(int connection_descriptor, char * username, FilesStruct * fs, char * delete_todo);

char * init_delete_todo(int size);
void set_delete_todo(int cfd, int message_num, int size, char * to_delete);
void reset_delete_todo(int cfd, int size, char * to_delete);

void retrieve_message(int cfd, int message_num, FilesStruct * fs, char * delete_todo, char * username);
void list_message_one(int cfd, int message_num, FilesStruct * fs, char * delete_todo);
void list_message_all(int cfd, FilesStruct * fs, char * delete_todo);
void stat_message(int cfd, FilesStruct * fs, char * delete_todo);

/*
*
* Definition of possible client commands for quick case-switching later on.
*
* popcmd	Command types.
*
* command_enums, command_strs and command_argreq are mappings between
* a command type to a combination of a string and an argument requirement
* where an argument requirement of 1 means one argument is present and
* an argument requirement of 0 means no argument is present
* 
* command_struct is a packaged version of a client command for easier processing
* 
*/
typedef enum {USER, PASS, QUIT, STAT, LIST_ALL, LIST_ONE, RETR, DELE, NOOP, RSET, ERR} popcmd;

int possible_commands_count = 10;
popcmd command_enums[] = { USER ,  PASS ,  QUIT ,  STAT ,  LIST_ALL,  LIST_ONE,  RETR ,  DELE ,  NOOP ,  RSET };
char* command_strs[]   = {"USER", "PASS", "QUIT", "STAT", "LIST"   , "LIST"   , "RETR", "DELE", "NOOP", "RSET"};
char command_argreq[]  = { true ,  true ,  false,  false,  false   ,  true    , true  ,  true ,  false,  false};

typedef struct cli_cmd_arg
{
	popcmd cmd;
	int cmdstr_index;
	char arg[ARG_SIZE+1];
}
command_struct;

command_struct get_command(int cfd);

/*
* This function is the starting point of the POP protocol
* on the given connection.
*/
void pop_protocol(int connection_descriptor)
{
	// Authorization State
	char username[ARG_SIZE];
	state_authorization(connection_descriptor, username);
	
	// Preparations
	FilesStruct * fs = dir_get_list(username);
	char * delete_todo = init_delete_todo(fs->count);
	
	// Transaction State
	state_transaction(connection_descriptor, username, fs, delete_todo);
	
	// Update State
	state_update(connection_descriptor, username, fs, delete_todo);
	
	// End session
	exit_pop(connection_descriptor, username, fs, delete_todo);
}

/*
* 	Ends the POP session and performs necessary cleanup.
*/
void exit_pop(int connection_descriptor, char * username, FilesStruct * fs, char * delete_todo)
{	
	log_write("POP", username, "Closing Connection", "");

	// Disconnect from client
	close_connection(connection_descriptor);
	
	// Clean-up
	if (fs != NULL)
	{
		sfree(fs);
	}
	if (delete_todo != NULL)
	{
		sfree(delete_todo);
	}

	// Exit thread
	pthread_exit(NULL);
}

/*
* 	Handler for the AUTHORIZATION state.
*/
void state_authorization(int cfd, char* username)
{
	command_struct command;
	int username_entered = false;

	socket_write(cfd, "+OK POP3 server ready.\r\n");
	
	while(1)
	{
		command = get_command(cfd);
		switch (command.cmd)
		{
			case USER:
				log_write("POP", "No Auth", "USER", command.arg);
				strncpy(username, command.arg, ARG_SIZE-1);
				username_entered = true;
				socket_write(cfd, "+OK Username received.\r\n");
				break;
			case PASS:
				log_write("POP", "No Auth", "PASS", command.arg);
				// Login successful
				if (username_entered && check_user(username, command.arg))
				{
					log_write("POP", username, "Authenticated", "");
					socket_write(cfd, "+OK Login successful.\r\n");
					return;
				}
				// Login unsuccessful
				else
				{
					if (username_entered)
					{
						log_write("POP", username, "Auth failed", "");
					}
					socket_write(cfd, "-ERR Invalid username/password.\r\n");
				}
				break;
			case QUIT:
				log_write("POP", "No Auth", "QUIT", "");
				socket_write(cfd, "+OK Login cancelled.\r\n");
				exit_pop(cfd, username, NULL, NULL);
			default:
				socket_write(cfd, "-ERR Invalid authorization command.\r\n");
				break;
		}
	}
}

/*
* 	Handler for the TRANSACTION state.
*/
void state_transaction(int cfd, char * username, FilesStruct * fs, char * delete_todo)
{
	log_write("POP", username, "TRANSACTION STATE", "");
	
	command_struct command;
	
	while(1)
	{
		command = get_command(cfd);
		log_write("POP", username, command_strs[command.cmdstr_index], command.arg);
		int message_num = 0;
		switch (command.cmd)
		{
			case STAT:
				stat_message(cfd, fs, delete_todo);
				break;
			case LIST_ALL:
				list_message_all(cfd, fs, delete_todo);
				break;
			case LIST_ONE:
				message_num = atoi(command.arg);
				list_message_one(cfd, message_num, fs, delete_todo);
				break;
			case RETR:
				message_num = atoi(command.arg);
				retrieve_message(cfd, message_num, fs, delete_todo, username);
				break;
			case DELE:
				message_num = atoi(command.arg);
				set_delete_todo(cfd, message_num, fs->count, delete_todo);
				break;
			case NOOP:
				socket_write(cfd, "+OK\r\n");
				break;
			case RSET:
				reset_delete_todo(cfd, fs->count, delete_todo);
				break;
			case QUIT:
				return;
			default:
				socket_write(cfd, "-ERR Invalid transaction command.\r\n");
				break;
		}
	}
}

/*
* 	Handler for the UPDATE state.
*/
void state_update(int cfd, char * username, FilesStruct * fs, char * delete_todo)
{
	log_write("POP", username, "UPDATE STATE", "");
	
	// Delete messages marked as deleted
	int i = 0;
	for (i = 0; i < fs->count; i++) {
		if (delete_todo[i] == true) {
			delete_mail(username, fs->FileNames[i]);
		}
	}
	
	socket_write(cfd, "+OK Thanks for logging in. Have a nice day.\r\n");
}

/*
* 	Initializes a 'to-do list' for mail deletion.
*/
char * init_delete_todo(int size)
{
	char * deltodo = (char *) smalloc(sizeof(char) * size);
	int i;
	for (i = 0; i < size; i++)
	{
		deltodo[i] = false;
	}
	return deltodo;
}

/*
* 	Marks a mail for deletion and notifies the client.
*/
void set_delete_todo(int cfd, int message_num, int size, char * to_delete)
{
	if (message_num > size || message_num <= 0) {
		socket_write(cfd, "-ERR Message entered does not exist. Please try again.\n");
	}
	else {
		to_delete[(message_num-1)] = true;
		socket_write(cfd, "+OK Marked for deletion.\n");
	}
}

/*
* 	Clears the mail deletion 'to-do list' and notifies the client.
*/
void reset_delete_todo(int cfd, int size, char * to_delete)
{
	int i;
	for (i = 0; i < size; i++) {
		to_delete[i] = false;
	}
	socket_write(cfd, "+OK Resetted.\r\n");
}

/*
* 	Given a mail number, sends the mail contents to the client.
*/
void retrieve_message(int cfd, int message_num, FilesStruct * fs, char * delete_todo, char * username)
{
	if (message_num > fs->count || message_num <= 0) {
		socket_write(cfd, "-ERR Cannot retrieve the selected message.\r\n");
		return;
	}
	if (delete_todo[message_num-1] == true) {
		socket_write(cfd, "-ERR Cannot retrieve the selected message.\r\n");
		return;
	}
	socket_write(cfd, "+OK Email follows.\r\n");
	char * message = get_file(username, fs->FileNames[message_num-1]);
	socket_write(cfd, message);
	sfree(message);
}

/*
* 	Given a mail number, sends a scan listing for that mail to the client.
*/
void list_message_one(int cfd, int message_num, FilesStruct * fs, char * delete_todo)
{
	char to_write[BUF_SIZE];
	if (message_num > fs->count || message_num <= 0) {
		socket_write(cfd, "-ERR Cannot retrieve the selected message.\r\n");
		return;
	}
	if (delete_todo[message_num-1] == true) {
		socket_write(cfd, "-ERR Cannot retrieve the selected message.\r\n");
		return;
	}
	snprintf(to_write, BUF_SIZE, "+OK %d %d\r\n", message_num, fs->FileSize[message_num-1]);
	socket_write(cfd, to_write);
}

/*
* 	Sends a scan listing for all available mail in the client's maildrop to the client.
*/
void list_message_all(int cfd, FilesStruct * fs, char * delete_todo)
{
	socket_write(cfd, "+OK Listing emails.\r\n");
	char to_write[BUF_SIZE];
	int i;
	for (i = 0; i < fs->count; i++) {
		if (delete_todo[i] == true) {
			continue;
		}
		snprintf(to_write, BUF_SIZE, "%d %d\r\n", i+1, fs->FileSize[i]);
		socket_write(cfd, to_write);
	}
	socket_write(cfd, ".\r\n");		
}

/*
* 	Sends a drop listing to the client.
*/
void stat_message(int cfd, FilesStruct * fs, char * delete_todo)
{
	int total_message = 0;
	int total_message_size = 0;
	int i = 0;
	char to_write[BUF_SIZE];
	for (i = 0; i < fs->count; i++) {
		if (delete_todo[i] == true) {
			continue;
		}
		total_message += 1;
		total_message_size += fs->FileSize[i];
	}
	snprintf(to_write, BUF_SIZE, "+OK %d %d\r\n", total_message, total_message_size); 
	socket_write(cfd, to_write);
}

/*
* 	Receives a command from the client and and packs it into a command_struct.
*/
command_struct get_command(int cfd)
{
	char response_buf[BUF_SIZE+1];
	char arg_buf[ARG_SIZE+1];
		
	bzero(response_buf, sizeof(response_buf));
	
	// Get response from socket
	char * response = socket_get_line(cfd);
	strncpy(response_buf, response, BUF_SIZE);
	sfree(response);
			
	// Match response to available options and wrap in a command_struct
	command_struct command;	
	
	int i;
	for (i = 0; i < possible_commands_count; i++)
	{
		bzero(arg_buf, sizeof(arg_buf));
		
		// Check command
		if (strncasecmp(response_buf, command_strs[i], CMD_SIZE) != 0)
		{
			continue;
		}
		
		// Should have argument
		if ((command_argreq[i] == true) && 
		    ((sscanf(response_buf, "%*s %s", arg_buf) != 1) || (response_buf[CMD_SIZE] != ' '))
		   )
		{
			continue;
		}
		// Should not have argument
		if ((command_argreq[i] == false) && (sscanf(response_buf, "%*s %s", arg_buf) != -1))
		{
			continue;
		}
		
		// Set command and return
		command.cmd = command_enums[i];
		command.cmdstr_index = i;
		if (command_argreq[i] == true)
		{
			strncpy(command.arg, arg_buf, ARG_SIZE);
		}
		return command;
	}
	
	// If no match then it's an invalid command
	command.cmd = ERR;
	return command;
}
