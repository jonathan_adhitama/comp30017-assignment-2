/*
*   COMP30017 Operating Systems and Network Services
*   Semester 1, 2013
*   Project 2
*
*   Team members:
*       Jonathan Adhitama Wongsodihardjo        jawo    531621
*       Michael Jonathan                        jonm    384881
*
*   Socket functions.
*
*/

#include "socket.h"
#include <strings.h>
#include <stdio.h>
#include <pthread.h>

#include <sys/socket.h>
#include <netinet/in.h>

#define BUFFER_SIZE 512
#define BUFFER_SIZE 512
#define LISTEN_BACKLOG 10

void* smalloc(size_t size);
void* srealloc(void *ptr, size_t size);
void sfree(void* ptr);

/*
* Creates a new socket and returns the socket descriptor.
*/
int socket_setup(int portno){
	// Create server socket (IPv4, TCP, default protocol)
	int socketfd = socket(AF_INET, SOCK_STREAM, 0);
	if (socketfd < 0)
	{
		perror("ERROR: Can't open socket.");
		exit(1);
	}
	
	// Create server socket address info
	struct sockaddr_in serv_addr;
	bzero((char*) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(portno);
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	
	// Bind server socket and its address info
	if (bind(socketfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0)
	{
		perror("ERROR: Can't bind server socket.");
		exit(1);
	}
	
	return socketfd;
}

/*
* Listens for a new connection,
* once there is a new connection
* the file descriptor of the new open connection is returned.
*/
int socket_get_new_connection(int sockfd){
	struct sockaddr_in cli_addr;
	int clilen = sizeof(cli_addr);
	int newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
	if (newsockfd < 0) 
	{
		perror("ERROR: Can't accept connection.");
		exit(1);
	}
	return newsockfd;
}

/*
* Reads a 'line' from the given connection.
* A 'line' means a sequence of characters ending with a CRLF pair.
* The string being returned must include the CRLF pair.
*/
char* socket_get_line(int fd){
	char * buffer = smalloc(BUFFER_SIZE);
	bzero(buffer, BUFFER_SIZE);
	int n = read(fd, buffer, (BUFFER_SIZE-1));
	if (n < 0) 
	{
		perror("ERROR: Can't read from socket.");
		pthread_exit(NULL);
	}
	return buffer;
}


/*
* Writes the given string to the given connection descriptor.
*/
void socket_write(int fd, char* string){
	int n = write(fd, string, strlen(string));
	if (n < 0) 
	{
		perror("ERROR: Can't write to socket.");
		pthread_exit(NULL);
	}
}


/*
* Closes the given connection descriptor.
*/
void close_connection(int fd){
	int status = close(fd);
	if (status < 0)
	{
		perror("ERROR: Can't close connection descriptor.");
		pthread_exit(NULL);
	}
}

/*
* Closes the given socket.
*/
void close_socket(int fd){
	int status = close(fd);
	if (status < 0)
	{
		perror("ERROR: Can't close socket.");
		exit(1);
	}
}
